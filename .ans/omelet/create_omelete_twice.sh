#!/usr/bin/env bash

#    Owner: Shai
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Date: 
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Version: 1.0.0
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Purpose: 
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


create_Omelet(){
local count=$1
local num='^[0-9]+$'
if ! [[ $count =~ $num ]] ; then
	$count="-O"
else
	let count++
fi
local eggs="()()"
local spices=('Salt' 'Pepper' 'Paprika')
local grease="Butter"
local cooking=""
cooking+="${grease} ${eggs} "
for i in "${spices[@]}"; do
	cooking+="${i} "
done
echo $count $cooking
}
create_Omelet 0
create_Omelet 1

