#!/usr/bin/env bash

#    Owner: Shai
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Date: 08/11/2020 
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Version: 1.0.0
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Purpose: in commit
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


show_menu(){ ## (1) Show menu
local meat=""
echo                                                                                                                 
echo "88                  88                     88         	 88                                                        "
echo "88                  88                     88          	 88                                                        "
echo "88                  88                     88         	 88                                                        "
echo "88   ,d8  ,adPPYba, 88,dPPYba,  ,adPPYYba, 88,dPPYba, 	 88,dPPYba,   ,adPPYba,  88       88 ,adPPYba,  ,adPPYba,  "
echo "88  ,a8  a8P_____88 88P     8a         Y8  88P      8a 	 88P      8a a8        8a88       88 I8[       a8P_____88  "
echo "8888[    8PP        88      d8 ,adPPPPP88  88       d8	 88       88 8b       d8 88       88   Y8ba,  8PP  	   "		
echo "88  Yba,  8b,   ,aa 88b,   ,a8   88,    ,88 88b,   a8 	 88       88  8a,   ,a8   8a,   ,a88 aa    ]8I 8b,   ,aa   "
echo "88    Y8a   Ybbd8   8Y Ybbd8      8bbdP Y8 8Y Ybbd8    	 88       88    YbbdP       YbbdP Y8   YbbdP      Ybbd8    "
echo
deco_simple "Please select animal"
## which animal sir?
	select animal in "Beef" "Lamb" "Pork" "Mix"; 
	 do
		case $animal in
			Beef) meat="Beef"; break;;
			Lamb) meat="Lamb"; break;;
			Pork) meat="Pork"; break;;
			Mix) meat="Mixed meat"; break;;
			*) _help ;;
		esac
	 done
## send to get_meat for processing (2)
get_meat "$meat" # (2)
get_spices "$return_meat" # (3) send the meat to process with spices
oven_cook "$return_mixed" # (4) mix them up & cook
pita_bread # (5) Prepare the pita and end the sale
echo $return_meat served in pita with $pita. Bon apetit
}

pita_bread(){ # (5) function to wrap all in a pita
local what_to_put="$@" # Get the cooked ingredients
deco_simple "ma limroaach?" # ask ma lasim in the pita
options=(" " "Hummus" "Chips Salad" "Hummus Chips Salad" "Amba Tahini" "Z-huck (Hariffff!!!)" "Sim li Hakol")
count=1
while [ $count -le 3 ]; do # create selection menu with options above
	deco_menu "$count" "${options[$count]}"
	let count++
done
echo
echo 
count=4
while [ $count -le 6 ]; do
	deco_menu "$count" "${options[$count]}"
	let count++
done
echo
echo
saveifs=$IFS
IFS= read -r opt
sauce=($what_to_put)
######################## get user's selection
if [[ $opt =~ ^[0-9]+$ ]]  && (( (opt >= 0) ));then 
	if (( opt == 6 )); then
		selected="Hummus Chips Salad Amba Tahini Zhuk"
	else selected="${options[$opt]}"
	fi
	echo Okay $selected
	sauce+="$selected"
else
	_help
fi
pita="${sauce[*]}" # save the selection in the pita
IFS=$saveifs
}
# (4) get the meat and ingredients for cooking
oven_cook(){
local food_process="$1" 
local low_temp="100" # temp in celsius
local med_temp="180"
local high_temp="220"
local cook_array=($low_temp $med_temp $high_temp)
local celsius=$'\xe2\x84\x83' # Celsius sign
for temp in "${cook_array[@]}"; do
	echo Cooking $food_process at $temp $celsius ## Cook in the oven
	sleep 2
done
}
## (3) Get mixed spices and meat from get_meat and get_spices
mix_all(){
local food_process="$@"
return_mixed=$(echo $food_process)
}
get_meat(){    ## Get meat from show_menu (1)
local which_meat="$1"
return_meat=$(echo $which_meat)
}
# (2)
get_spices(){ ## Get meat and send the spices to mixture at (3)
local which_meat="$1"
local spices="Salt,Pepper,Pitrushka"
return_spices=$(echo $spices)
return_meat=$(echo $which_meat)
mix_all "$return_spices" "$return_meat" # send to mixture at (3)
}
deco(){
_time=2.5
clear
printf "$l\n# %s\n$l\n" "$@"
sleep $_time
clear
}

deco_menu(){
printf "%s) %s      " "$@"
}
deco_simple(){
printf "$l\n# %s\n$l\n" "$@"
}
_help(){
if [[ -z $@ ]];then
   deco  "please provide answer"
   exit 1
else
deco "$@"
fi
}
################# Do not remove ################
show_menu
