# Welcome  To Your Shell Kitchen, Bashef.

Below you'll see list of cuisine dishes that might be interesting for tonights guests:

### [Aunt Mary's Cake of Permissions](./aunt_marys_cakes_of_permissions/README.md)

### [Cookie Jar](./cookie_jar/README.md)

### [New York Shell Steak](./new_york_shell_steak/README.md)

### [Omelet](./omelet/README.md)

### [Regex Salad](./regex_salad/README.md)

### [Shell Kebab](./shell_kebab/README.md)

### [Spaggetti Bash](./spaggetti_bash/README.md)

